/**
 * Created by Gabor on 2017. 01. 18..
 */

$(document).ready(function(){
    $("#basket").click(function (e){

        e.preventDefault();

        var cart = $($(this).data('target'));

        if(cart.is(':visible')){
            cart.hide();
        }else{
            cart.show();
        }

    });

    $("body").delegate('.cart.add', 'click', function(e){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();

        $.ajax({
            type: "POST",
            url: this.href,
            data: {id: $(this).data('id'), quantity: 1},
            dataType : "json",
            success : function(data){

                if(data !== false){

                    $("#cart").replaceWith(data);

                }

            }
        }, 'json');

    });

    $("body").delegate('.cart.remove', 'click', function(e){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();

        $.ajax({
            type: "POST",
            url: this.href,
            data: {id: $(this).data('id')},
            dataType : "json",
            success : function(data){

                if(data !== false){

                    $("#cart").replaceWith(data);

                }

            }
        }, 'json');

    });

    $("body").delegate('.cart.delete', 'click', function(e){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();

        $.ajax({
            type: "POST",
            url: this.href,
            data: {},
            dataType : "json",
            success : function(data){

                if(data !== false){

                    $("#cart").replaceWith(data);

                }

            }
        }, 'json');

    });

});