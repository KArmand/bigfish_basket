<?php

/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 17.
 * Time: 17:24
 */

use App\Status;
use Illuminate\Database\Seeder;

class DiscountedProductTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ids = array(1001, 1003, 1004, 1005);

        $discount_products = array(
            (object) array('product' => 1006 ,"discount" => 101),
            (object) array('product' => 1002 ,"discount" => 102)
        );

        foreach($ids as $id){

            $discount_products[] = (object) array('product' => $id ,"discount" => 103);

        }

        foreach($discount_products as $dp){

            DB::table("discounted_product")->insert([
                'product' => $dp->product,
                'discount' => $dp->discount,
                'active' => Status::ACTIVE
            ]);

        }

    }

}