<?php

/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 17.
 * Time: 17:53
 */

use Illuminate\Database\Seeder;
use App\Status;

class ProductTableSeeder extends Seeder
{

    public function run()
    {

        $products = array(
            (object) array('id' => 1001, 'title' => 'Dreamweaver CS4', 'author' => 'Janine Warner', 'publisher' => 1, 'price' => 3900),
            (object) array('id' => 1002, 'title' => 'JavaScript kliens oldalon', 'author' => 'Sikos László', 'publisher' => 2, 'price' => 2900),
            (object) array('id' => 1003, 'title' => 'Java', 'author' => 'Barry Burd', 'publisher' => 1, 'price' => 3700),
            (object) array('id' => 1004, 'title' => 'C# 2008', 'author' => 'Stephen Randy Davis', 'publisher' => 1, 'price' => 3700),
            (object) array('id' => 1005, 'title' => 'Az Ajax alapjai', 'author' => 'Joshua Eichorn', 'publisher' => 1, 'price' => 4500),
            (object) array('id' => 1006, 'title' => 'Algoritmusok', 'author' => 'Ivanyos Gábor, Rónyai Lajos, Szabó Réka', 'publisher' => 3, 'price' => 3600)
        );

        foreach($products as $product){

            DB::table("product")->insert([
                'id' => $product->id,
                'title' => $product->title,
                'author' => $product->author,
                'price' => $product->price,
                'public' => Status::ACTIVE,
                'publisher' => $product->publisher
            ]);

        }

    }

}