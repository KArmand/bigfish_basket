<?php

/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 17.
 * Time: 17:53
 */

use Illuminate\Database\Seeder;

class PublisherTableSeeder extends Seeder
{

    public function run()
    {

        $publishers = array(
            (object) array('name' => "PANEM"),
            (object) array('name' => "BBS-INFO"),
            (object) array('name' => "TYPOTEX")
        );

        foreach($publishers as $publisher){

            DB::table("publisher")->insert([
                'name' => $publisher->name
            ]);

        }

    }

}