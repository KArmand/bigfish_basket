<?php

/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 17.
 * Time: 17:24
 */

use Illuminate\Database\Seeder;

class DiscountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $discounts = array(
            (object) array('id' => 101, 'type' => "10%-os kedvezmény a termék árából"),
            (object) array('id' => 102, 'type' => "termék, 500-os kedvezmény a termék árából"),
            (object) array('id' => 103, 'type' => "2+1 csomag kedvezmény (a szettben szereplő legolcsóbb termék 100%-os kedvezményt kap)")
        );

        foreach($discounts as $discount){

            DB::table("discount")->insert([
                'id' => $discount->id,
                'type' => $discount->type
            ]);

        }

    }

}