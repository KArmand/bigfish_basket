<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 18.
 * Time: 20:13
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class DiscountedProduct extends Model
{

    protected $table = 'discounted_product';

    protected $primaryKey = 'id';

    public function getProduct(){
        return $this->hasOne('\App\Product', 'id', 'product');
    }

    public function getDiscount(){
        return $this->hasOne('\App\Discount', 'id', 'discount');
    }

}