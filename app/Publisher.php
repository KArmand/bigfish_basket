<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 18.
 * Time: 18:40
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{

    protected $table = 'publisher';

    protected $primaryKey = 'id';

}