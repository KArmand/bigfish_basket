<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 19.
 * Time: 14:38
 */

namespace App;


use Illuminate\Support\Facades\Session;

class Cart
{

    const SESSION_NAME = 'cart.items';
    const SESSION_DISCOUNTED_NAME = 'cart.discount';

    /**
     * @return array
     *
     * return cart items
     *
     */
    public function get_all(){

        return Session::has(self::SESSION_NAME) ? Session::get(self::SESSION_NAME) : array();
    }

    /**
     * @param CartItem $item
     *
     * add cart by id or update cart item quantity
     *
     */
    public function add(CartItem $item){

        //ha mar letezik
        if(Session::has(Cart::SESSION_NAME)) {

            $cart = Session::get(Cart::SESSION_NAME);

            if (isset($cart[$item->id])) {

                $cart[$item->id]->quantity += $item->quantity;
                Session::set(Cart::SESSION_NAME, $cart);

            }

        }

        Session::put(self::SESSION_NAME, array_add(Session::get(Cart::SESSION_NAME), $item->id, (object) $item));
        Session::forget(self::SESSION_DISCOUNTED_NAME);
    }

    /**
     * @return bool|string
     */
    public function get_sum(){

        $items = $this->get_all();

        if(empty($items)) return false;

        $sum = $discountsum = 0;

        $panem = array();

        foreach ($items as $item){

            //kulon tombbe gyujtom a panam termekeket
            if($item->discount == Discount::PANEM && $item->quantity > 1){
                for($i=1;$i<=$item->quantity;$i++){
                    $panem[] = $item->price;
                }
            }elseif($item->discount == Discount::PANEM){
                $panem[] = $item->price;
            }

            $sum += $item->product->getDiscountPriceWithoutFormat * $item->quantity;

            $discountsum += (($item->product->price - $item->product->getDiscountPriceWithoutFormat)  * $item->quantity);

        }

        //ha 2+1 akcio ervenyben van, kivonom a legolcsobbat
        if(sizeof($panem) > 2){

            array_multisort($panem);

            return '<small><s>(' . number_format($sum, 0, '', ' ') . ')</s></small> <h5>- ' . number_format($discountsum + $panem[0], 0, '', ' ') . '</h5> ' . number_format($sum - $panem[0], 0, '', ' ');

        }

        return number_format($sum, 0, '', ' ');

    }

    /**
     * @param $id
     *
     * remove cart item by id
     *
     */
    public function remove($id){

        $cart = Session::get(Cart::SESSION_NAME);
        unset($cart[$id]);
        Session::set(Cart::SESSION_NAME, $cart);
        Session::forget(self::SESSION_DISCOUNTED_NAME);
    }

    /**
     * delete alll cart item
     */
    public function delete(){

        Session::forget(self::SESSION_NAME);
        Session::forget(self::SESSION_DISCOUNTED_NAME);

    }

}