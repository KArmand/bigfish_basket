<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 17.
 * Time: 17:43
 */

namespace App;


class Status
{
    const ACTIVE = 1;
    const INACTIVE = 0;
}