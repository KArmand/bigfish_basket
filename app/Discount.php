<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 18.
 * Time: 20:21
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{

    protected $table = 'discount';

    protected $primaryKey = 'id';

    const PANEM = 103;

}