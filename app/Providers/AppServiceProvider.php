<?php

namespace App\Providers;

use App\Cart;
use App\CartItem;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('includes.cart', function ($view)
        {
            $cart = new Cart();

            $view->with(array(
                'cart_items' =>  $cart->get_all(),
                'cart_sum' => $cart->get_sum()
            ));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
