<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 19.
 * Time: 16:32
 */

namespace App\Http\Controllers;


use App\Cart;
use App\CartItem;
use App\Product;
use Illuminate\Http\Request;
use League\Flysystem\Exception;

class CartController extends Controller
{
    /**
     * @param Request $request
     * @return json or false
     */
    public function add(Request $request){

        try {

            $product = Product::findOrFail($request->id);

            $cart = new Cart();

            $cartitem = new CartItem($product->id, $product->title, $product->price, $product->getDiscount->getDiscount->id, $product, (int) $request->quantity);

            $cart->add($cartitem);

        }catch (Exception $e){

            return json_encode(FALSE);

        }

        return json_encode((string) view('includes.cart')->render());

    }

    /**
     * @param Request $request
     * @return json or false
     */
    public function remove(Request $request){

        try {

            $product = Product::findOrFail($request->id);

            $cart = new Cart();

            $cart->remove($product->id);

        }catch (Exception $e){

            return json_encode(FALSE);

        }

        return json_encode((string) view('includes.cart')->render());

    }

    /**
     * @return json
     */
    public function delete(){

        $cart = new Cart();
        $cart->delete();

        return json_encode((string) view('includes.cart')->render());

    }

}