<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 18.
 * Time: 18:31
 */

namespace App\Http\Controllers;

use App\Cart;
use App\CartItem;
use App\Product;
use App\Status;
use Illuminate\Support\Facades\Session;

class ListController extends Controller
{

    public function index(){

        $products = Product::has('getPublisher')
            ->where('public', Status::ACTIVE)
            ->orderBy('title', 'asc')
            ->get();

        return view("pages/list", ['products' => $products]);

    }

}