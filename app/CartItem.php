<?php
/**
 * Created by PhpStorm.
 * User: Gabor
 * Date: 2017. 01. 19.
 * Time: 14:38
 */

namespace App;
use Illuminate\Support\Facades\Session;


/**
 * Class CartItem
 * @package App
 */
class CartItem
{
    /**
     * @vars
     */
    public $price, $id, $title, $discount, $quantity, $product;

    /**
     * CartItem constructor.
     * @param int $id
     * @param string $title
     * @param int $price
     * @param int $discount
     * @param int $quantity, default 1
     */
    public function __construct($id, $title, $price, $discount, $product,  $quantity = 1)
    {
        if(empty($id)) {
            throw new \InvalidArgumentException('Please supply a valid identifier.');
        }
        if(empty($title)) {
            throw new \InvalidArgumentException('Please supply a valid name.');
        }
        if(strlen($price) < 1) {
            throw new \InvalidArgumentException('Please supply a valid price.');
        }
        if(empty($discount)) {
            throw new \InvalidArgumentException('Please supply a valid discount.');
        }
        if((int) $quantity < Status::ACTIVE) {
            throw new \InvalidArgumentException('Please supply a valid quantity.');
        }
        if(!$product instanceof Product) {
            throw new \InvalidArgumentException('Please supply a valid product.');
        }

        $this->id       = $id;
        $this->title    = $title;
        $this->price    = (int) $price;
        $this->discount = $discount;
        $this->quantity = $quantity;
        $this->product = $product;

    }

    /**
     * @return CartItem
     */
    public function get(){
        return $this;
    }

    public function getFullPrice(){

        $cart = new Cart();

        $panem = array();

        foreach ($cart->get_all() as $item){

            if($item->discount == Discount::PANEM && $item->quantity > 2){

                for($i=0;$i<=$item->quantity;$i++){
                    $panem[] = $item->price;
                }

            }elseif($item->discount == Discount::PANEM){

                $panem[] = $item->price;

            }

        }

        $quantity = $this->quantity;

        if(sizeof($panem) > 0){

            array_multisort($panem);

            if($panem[0] == $this->product->price && sizeof($panem) > 2 && Session::has(Cart::SESSION_DISCOUNTED_NAME) === FALSE){
                $quantity = $quantity - 1;
                //csak egyszer kell az akciot ervenyesíteni
                Session::put(Cart::SESSION_DISCOUNTED_NAME, TRUE);
            }
        }

        return $this->product->getDiscountPriceWithoutFormat * $quantity;

    }


}