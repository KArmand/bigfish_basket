#Bigfish Próbafeladat - Garamvölgyi Gábor 

##A feladat leírása

####Termék lista oldal

- termékek listájának megjelenítése ABC vagy ár szerint növekvő sorrendben,

- a listában szereplő termékeknek minden adatát - amennyiben a termék kedvezményes akkor az eredeti és a kedvezményes árát is - meg kell jeleníteni, 

####Kosár doboz 

- a listából egyesével kosárba helyezett termékek aktuális, kedvezményekkel csökkentett összértékének megjelenítése,

####Kosár lista

- kosárban lévő termékek lista jellegű megjelenítése,

- a lista alatt szerepelnie kell a kosár kedvezmények nélkül összesített értékének, a kedvezmények összértékének és a kedvezményekkel csökkentett végösszegnek,

- a felhasználónak lehetősége van a kosarát elmenteni és visszaolvasni valamint üríteni (egy sesssion alatt egy kosár mentését kell megoldani, a második mentési esemény felül kell, hogy írja a tárolt adatokat)

Az elképzelt architektúra szerint a webshop minden műveletet AJAX hívásokkal valósít meg és a szükséges adatokat szabványos HTTP REST API-n keresztül szerzi be a háttérrendszertől. A háttérrendszer az adatokat XML adatfile(ok)-ban vagy adatbázisban tárolja.

A háttérrendszer végzi a kosár adatok alapján a termékek árának és a kosár végösszegének kalkulációját. A frontend oldali dinamikus kosár kezelés mellett a feladat része egy olyan egységteszt (unit test) elkészítése amivel meghatározott kosár konfigurációk végösszegét lehet ellenőrizni.

##uml diagram

[megtekintés](https://app.box.com/s/1t1mlwqky8fmmfi4op2mfwg9dbo4r5d6)

##Telepítési útmutaó

1. Adatbázis és db felhasználó létrehozása
2. Virtual host beállítása a projekt/public mappára
3. .env létrehozása és beállítása
4. composer update lefuttatása
5. (production módban külön lefuttatni: php artisan migrate --seed)