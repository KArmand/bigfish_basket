@extends('main')

@section('content')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">BIG FISH PHP PRÓBAFELADAT</h1>
            <br />
            <ul>
                <li>kosárban lévő termékek lista jellegű megjelenítése,</li>
                <li>a lista alatt szerepelnie kell a kosár kedvezmények nélkül összesített értékének, a kedvezmények összértékének és a kedvezményekkel csökkentett végösszegnek</li>
                <li>a felhasználónak lehetősége van a kosarát elmenteni és visszaolvasni valamint üríteni (egy sesssion alatt egy kosár mentését kell megoldani, a második mentési esemény felül kell, hogy írja a tárolt adatokat)</li>
            </ul>
        </div>
    </div>

    <div class="container marketing">
        <div class="row">
            <div class="col-md-6">
                <p>Az elképzelt architektúra szerint a webshop minden műveletet AJAX hívásokkal

                    valósít meg és a szükséges adatokat szabványos HTTP REST API-n keresztül szerzi

                    be a háttérrendszertől. A háttérrendszer az adatokat XML adatfile(ok)-ban vagy

                    adatbázisban tárolja.

                    </p>
            </div>
            <div class="col-md-6">
                <p>A háttérrendszer végzi a kosár adatok alapján a termékek árának és a kosár

                    végösszegének kalkulációját. A frontend oldali dinamikus kosár kezelés mellett a

                    feladat része egy olyan egységteszt (unit test) elkészítése amivel meghatározott

                    kosár konfigurációk végösszegét lehet ellenőrizni.</p>
            </div>
        </div>
    </div>

@endsection