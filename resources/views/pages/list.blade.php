@extends('main')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Cím</th>
                            <th>Author</th>
                            <th>Ára</th>
                            <th>Kedvezmény</th>
                            <th>Szerző</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>

                    <tbody>

                    @forelse ($products as $product)

                        <tr>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->author }}</td>
                            <td>{{ $product->getPrice }} HUF</td>
                            <td><small><strong>{{ $product->getDiscountPrice }}</strong></small></td>
                            <td>{{ $product->getPublisher->name }}</td>
                            <td><a href="{{ route('cart.add') }}" class="btn btn-primary cart add" data-id="{{ $product->id }}">Kosárba teszem</a></td>
                        </tr>

                    @empty
                        <tr>
                            <td colspan="10">Nincs találat</td>
                        </tr>
                    @endforelse

                    </tbody>

                </table>
            </div>
        </div>

    </div>
@endsection