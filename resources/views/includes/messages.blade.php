@if (count($errors) > 0)
<section>
    <div class = "alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</section>
@endif

@foreach (['warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
<div class="alert alert-{{ $msg }}">
    <button type="button" class="close" data-dismiss="alert" aria-label="Bezárás">
        <i class="fa fa-times"></i>
    </button>
    {{ Session::get('alert-' . $msg) }}
</div>
    @endif
@endforeach
