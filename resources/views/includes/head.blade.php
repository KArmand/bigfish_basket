<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="shortcut icon" href="{{ url('favicon.ico') }}" type="image/x-icon" />

<title>{{ $title or 'The BigFish App - Garamvölgyi Gábor' }}</title>

<!-- Bootstrap core CSS -->
<link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('css/style.css') }}" rel="stylesheet">


<!-- Axio css -->
<link rel="stylesheet" href="{{ url('css/style.css') }}" />