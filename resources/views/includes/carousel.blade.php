@if(!$carousels->isEmpty())

<?php $carouselID = 'my_carousel_' . str_random(30); ?>
<!-- Carousel
================================================== -->
<div id="{{ $carouselID }}" class="carousel slide hidden-md-down" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    @foreach ($carousels as $key => $carousel)
        <li data-target="#{{ $carouselID }}" data-slide-to="{{ $key }}" class="{{ $key == 1 ? 'active' : '' }}"></li>
    @endforeach
    </ol>
    <div class="carousel-inner" role="listbox">

    @foreach ($carousels as $key => $carousel)

        <div class="carousel-item{{ $key == 0 ? ' active' : '' }}">
            <img class="first-slide" src="http://placehold.it/1900x500/72b626/fefefe" alt="First slide">
            <div class="container">
                <div class="carousel-caption text-xs-left">
                    <h1>{{ $carousel->name }}</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                </div>
            </div>
        </div>

    @endforeach
    </div>

    <a class="left carousel-control" href="#{{ $carouselID }}" role="button" data-slide="prev">
        <span class="icon-prev" aria-hidden="true"></span>
        <span class="sr-only">Előző</span>
    </a>

    <a class="right carousel-control" href="#{{ $carouselID }}" role="button" data-slide="next">
        <span class="icon-next" aria-hidden="true"></span>
        <span class="sr-only">Következő</span>
    </a>

</div><!-- End Of Carousel -->
@endif