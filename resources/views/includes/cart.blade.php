<div id="cart">
    <table class="table table-striped">
        @if($cart_items)
        <thead>
            <tr>
               <td>Cím</td>
               <td>Ár</td>
               <td>Kedvezményes ár</td>
               <td>Darabszám</td>
               <td>&nbsp;</td>
            </tr>
        </thead>
        @endif

        @forelse ($cart_items as $item)

            <tr>
                <td>{{ $item->title }}</td>
                <td>{{ $item->price * $item->quantity }} HUF</td>
                <td><small><strong>{{ $item->getFullPrice() }} HUF</strong></small></td>
                <td>{{ $item->quantity }}</td>
                <td><a href="{{ route('cart.remove') }}" data-id="{{ $item->id }}" class="btn btn-warning cart remove">törlés</a></td>
            </tr>

        @empty
            <tr>
                <td colspan="10" class="text-center">Az Ön kosara üres.</td>
            </tr>
        @endforelse

        @if($cart_items)
            <tr>
                <td colspan="10" class="text-right">
                    <strong>Összesen: {!! $cart_sum !!} HUF</strong>
                </td>
            </tr>
            <tr>
                <td colspan="10" class="text-right">
                    <a href="{{ route('cart.delete') }}" class="btn btn-danger cart delete">Kosár üritése</a>
                </td>
            </tr>
        @endif

    </table>

</div>