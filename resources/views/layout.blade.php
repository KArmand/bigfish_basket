<!DOCTYPE html>
<html lang="hu">

<head>
    @include('public.includes.head')
</head>

<body>

@include('public.includes.header')

@include('public.includes.messages')

<!-- CONTENT -->
@yield('content')
<!-- END OF CONTENT -->

<!-- FOOTER -->
@include('public.includes.footer')

<!-- Bootstrap core JavaScript
        ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ url('bower_components/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('bower_components/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ url('bower_components/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>

@stack('scripts')

</body>
</html>
