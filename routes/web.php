<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/welcome');
});

Route::get('list', [
    'as' => 'list', 'uses' => 'ListController@index'
]);

Route::post('cart/add' ,[
    'as' => 'cart.add',
    'uses'  => 'CartController@add'
]);

Route::post('cart/delete' ,[
    'as' => 'cart.delete',
    'uses'  => 'CartController@delete'
]);

Route::post('cart/remove' ,[
    'as' => 'cart.remove',
    'uses'  => 'CartController@remove'
]);